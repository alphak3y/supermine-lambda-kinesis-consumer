import base64
import json
from types import LambdaType
import mysql.connector
import os
#import boto3
from lambda_cache import secrets_manager
#from botocore.exceptions import ClientError
from enum import Enum
import sys

class LambdaException(Exception):
    pass

class farmer_tokens(Enum):
    farmer_token    = 0
    xch_next_ppm    = 1
    user_name       = 2
    xch_points      = 3
    time_last_check = 4
    status          = 5
    xch_fingerprint = 6
    name            = 7
    hdd_next_ppm    = 8
    hdd_fingerprint = 9
    hdd_points      = 10

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    for record in event['Records']:
        payload = base64.b64decode(record["kinesis"]["data"])
        arrival = record["kinesis"]["approximateArrivalTimestamp"]

        arrival   = int(arrival * 1000)
        jsonreq   = {}
        token     = ""
        chainlist = {}
        lasttime = None
        updatepoints = True

        try:
            jsonreq = json.loads(payload)
            token = jsonreq["farmer_token"]
            chainlist = jsonreq["plot_count"]
        except:
            print("ERROR decoding JSON: " + str(sys.exc_info()[0]))
            continue

        try:
            sql = "SELECT * FROM farmer_tokens WHERE farmer_token = %s"
            val = (token,)
            cursor.execute(sql, val)
            result = cursor.fetchone()

            if result is None:
                print("No entry found for FARMER_TOKEN '" + token + "'.")
                continue
            xch_nextppm = result[farmer_tokens.xch_next_ppm.value]
            hdd_nextppm = result[farmer_tokens.hdd_next_ppm.value]
            lasttime = result[farmer_tokens.time_last_check.value]
        except mysql.connector.Error as error:
            print(error)
            continue

        if lasttime is None:
            continue

        if (arrival - lasttime) < 60000:
            updatepoints = False
            print("Farmer initing too much: " + token)
        if (arrival - lasttime) > 180000:
            updatepoints = False
            print("Farmer inited after over 3 minutes: " + token)

        # updates time
        try:
            sql = "UPDATE farmer_tokens SET time_last_check = %s WHERE farmer_token = %s"
            val = (arrival, token)
            cursor.execute(sql, val)
        except mysql.connector.Error as error:
            print(error)
            continue

        minutes = (arrival - lasttime) / 60000

        for chain in chainlist:
            ppm = 0
            currpoints = 0

            if updatepoints:
                if "xch" in chain: points = xch_nextppm * minutes
                elif "hdd" in chain: points = hdd_nextppm * minutes

                points_chain = chain + "_points"

                # get points
                try:
                    sql = "SELECT " + points_chain + " FROM farmer_tokens WHERE farmer_token = %s"
                    val = (token,)
                    cursor.execute(sql, val)
                    result = cursor.fetchone()
                    currpoints = result[0]
                except mysql.connector.Error as error:
                    print(error)
                    continue

                points += currpoints
                # updates points 
                try:
                    sql = "UPDATE farmer_tokens SET " + points_chain + " = %s WHERE farmer_token = %s"
                    val = (points, token)
                    cursor.execute(sql, val)
                except mysql.connector.Error as error:
                    print(error) 
                    continue

            for harvester in chainlist[chain]:
                for size in chainlist[chain][harvester]:
                    amt = int(chainlist[chain][harvester][size])
                    size = int(size)
                    
                    if size >= 32 and size <= 35:
                        points = 2 ** (size - 32)
                    else:
                        print("Plot size invalid! k: " + str(size) + " amt: " + str(amt) + " farmer_token: " + str(token))
                        raise LambdaException(500)
                    ppm += (amt * points)

                    hrv_chain = chain + "_remote_harvester"
                    try:
                        sql = "UPDATE " + hrv_chain + " SET owned_plots = %s WHERE remote_harvester = %s"
                        val = (amt, harvester)
                        cursor.execute(sql, val)
                    except mysql.connector.Error as error:
                        print(error)
                        continue
                next_ppm_chain = chain + "_next_ppm"
                try:
                    sql = "UPDATE farmer_tokens SET " + next_ppm_chain + " = %s WHERE farmer_token = %s"
                    val = (ppm, token)
                    cursor.execute(sql, val)
                except mysql.connector.Error as error:
                    print(error)
                    continue

    db.commit()
    cursor.close()
    db.close()

    return {
        'message': 'Farmer message processed successfully.'
    }