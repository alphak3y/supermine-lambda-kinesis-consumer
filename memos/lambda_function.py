import mysql.connector
import time
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["farmer_token"] is None:
        raise LambdaException(404)

    token = event["farmer_token"]

    rightnow = round(time.time() * 1000)

    for key in event:
        if key == "farmer_token":
            continue

        chain = key
        pid_chain = chain + "_plot_ids"
        for harvester in event[chain]:
            for pid in event[chain][harvester]:
                try:
                    sql = "INSERT INTO " + pid_chain + " (plot_id, farmer_token, remote_harvester, time_last_check) VALUES (%s, %s, %s, %s)"
                    val = (pid, token, harvester, rightnow)
                    cursor.execute(sql, val)
                except mysql.connector.Error as error:
                    try:
                        sql = "SELECT remote_harvester FROM " + pid_chain + " WHERE plot_id = %s"
                        val = (pid,)
                        cursor.execute(sql, val)
                        result = cursor.fetchone()
                        assigned_harvester = result[0]

                        try:
                            sql = "SELECT time_last_check FROM " + pid_chain + " WHERE plot_id = %s"
                            val = (pid,)
                            cursor.execute(sql, val)
                            result = cursor.fetchone()
                            if result is None:
                                raise LambdaException(400)

                            lasttime = result[0]

                            if (rightnow - lasttime) < 180000:
                                if assigned_harvester != harvester:
                                    cursor.close()
                                    db.close()
                                    print("user using the same pid on different harvester")
                                    raise LambdaException(400)
                        except mysql.connector.Error as error:
                            cursor.close()
                            db.close()
                            raise LambdaException(400)
                    except mysql.connector.Error as error:
                        cursor.close()
                        db.close()
                        raise LambdaException(400)

                    try:
                        sql = "UPDATE " + pid_chain + " SET time_last_check = %s WHERE plot_id = %s"
                        val = (rightnow, pid)
                        cursor.execute(sql, val)
                    except mysql.connector.Error as error:
                        cursor.close()
                        db.close()
                        raise LambdaException(400)

    db.commit()

    cursor.close()
    db.close()

    return {
        'body': {
          'message': 'Inserted data.'
        }
    }