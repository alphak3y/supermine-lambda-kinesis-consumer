import time
import sslcrypto
import mysql.connector
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    
    cursor = db.cursor()

    if event["login_token"] is None:
        raise LambdaException("404")

    logintoken = event["login_token"]
    tokenname = event["token_name"]
    if tokenname is None:
        tokenname = "My Farmer Token"
    username = ""

    try:
        sql = "SELECT user_name FROM sessions WHERE login_token = %s"
        val = (logintoken,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    public_key = b'\x033\xc7\xdfVH1\x03\xf6K+\x9d2\xac$6\xcb\xbfN\xe7Oi\x8cM\xe7\xee\xb4\xe5a\xd4*\x80U'

    rightnow = str(round(time.time() * 1000))
    hash = rightnow + username

    # encode
    curve = sslcrypto.ecc.get_curve("secp256k1")
    ciphertext = curve.encrypt(hash, public_key, algo="aes-256-ctr")

    new_farmer_token = ciphertext.hex()


    sql = "INSERT INTO farmer_tokens (farmer_token, user_name, name) VALUES (%s, %s, %s)"
    val = (new_farmer_token, username, tokenname)
    cursor.execute(sql, val)

    db.commit()

    cursor.close()
    db.close()

    return {
        'body': {
          'farmer_token': new_farmer_token
        }
    }

