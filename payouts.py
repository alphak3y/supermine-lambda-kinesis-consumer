import os
from re import U
from subprocess import check_output
import subprocess
import mysql.connector
from enum import Enum
import boto3
from botocore.exceptions import ClientError
import json

class Fees(Enum):
    user = 0.98 # user
    beta = 0.99 # beta tester
    alpha = 1.00 # alpha tester

class farmer_tokens(Enum):
    farmer_token    = 0
    xch_next_ppm    = 1
    user_name       = 2
    xch_points      = 3
    time_last_check = 4
    status          = 5
    xch_fingerprint = 6
    name            = 7
    xfx_next_ppm    = 8
    xfx_fingerprint = 9
    xfx_points      = 10

class Users(Enum):
    user_name        = 0
    password         = 1
    xch_balance      = 2
    xch_fee_status   = 3
    xch_reward_addr  = 4
    xch_auto_payout  = 5
    banned           = 6
    gas              = 7
    xch_lifetime_bal = 8
    xfx_balance      = 9
    xfx_fee_status   = 10
    xfx_reward_addr  = 11 
    xfx_auto_payout  = 12
    xfx_lifetime_bal = 13

def writeToLog(output, file):
    with open('home/chia/logs/' + file, 'w') as f:
        sys.stdout = f # Change the standard output to the file we created.
        writeLogFile(output)
        sys.stdout = original_stdout

def syncPayouts():
    transaction = subprocess.run(["/home/chia/chia-blockchain/venv/bin/chia", "wallet", "show"], capture_output=True)

    for line in transaction.stdout:
        if 'sync status' in line:
            if 'not' in line:
                writeLogFile('The Payouts EC2 is not in sync.\nWe will have to wait before processing payments.','sync_errors.log')
                return
            else:
                Payout()


def Payout():
    db = mysql.connector.connect(
        host="farmer-database.cydm5y3aznbz.us-east-1.rds.amazonaws.com",
        user=get_secret("rds_username"),
        password=get_secret("rds_password"),
        database="xch_farmer_database",
        port=5400
    )
    cursor = db.cursor()

    lastbal = None
    totalpoints = 0
    bal = 0.0
    addedbal = 0.0

#    with open("logs.txt", "rb") as file:
#        file.seek(-2, os.SEEK_END)
#        while file.read(1) != b"n":
#            file.seek(-2, os.SEEK_CUR)
#        lastbal = file.readline().decode()

#    wallet = subprocess.Popen("chia wallet show > log.txt", shell=True).wait()
#    file = open("log.txt", "r")
#    for line in file:
#        if "Total Balance" in line:
#            line.replace(" ", "")
#            line.split("(")[0]
#            bal = float(line[14:-3])

#    earned = bal - lastbal

    #test = subprocess.run(["ls", "-l", "/home/jiyouzu/hpool"], capture_output=True)

    #writeLogFile(test)

    earned = 2


    try:
        sql = "SELECT SUM(points) FROM farmer_tokens"
        cursor.execute(sql)
        result = cursor.fetchone()

        totalpoints = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        writeLogFile("Failed to sum points.", sql_errors.log)

    rewardperpoint = float(earned / totalpoints)

    try:
        sql = "SELECT * FROM farmer_tokens"
        cursor.execute(sql)
        all_farmer_tokens = cursor.fetchall()

        for row in all_farmer_tokens:
            points = row[farmer_tokens.points.value]
            username = row[farmer_tokens.user_name.value]

            try:
                sql = "SELECT * FROM users WHERE user_name = %s"
                val = (username,)
                cursor.execute(sql, val)
                result = cursor.fetchone()

                curbal = float(result[Users.balance.value])
                feestatus = result[Users.fee_status.value]

                if feestatus == "user":    addedbal = (points * rewardperpoint) * Fees.user.value
                elif feestatus == "beta":  addedbal = (points * rewardperpoint) * Fees.beta.value
                elif feestatus == "alpha": addedbal = (points * rewardperpoint) * Fees.alpha.value
                else:
                    writeLogFile("Invalid fee status for user" + username + ".",'sql_errors.log')
                    continue

                newbal = curbal + addedbal

                try:
                    sql = "UPDATE users SET balance = %s WHERE user_name = %s"
                    val = (newbal, username)
                    cursor.execute(sql, val)

                except mysql.connector.Error:
                    writeLogFile("Failed updating balance for user " + username + " before payout.", 'sql_errors.log')
                    continue

                try:
                    sql = "UPDATE farmer_tokens SET points = 0 WHERE user_name = %s"
                    val = (username,)
                    cursor.execute(sql, val)

                except mysql.connector.Error:
                    writeLogFile("Failed updating points for user " + username + " before payout.",'sql_errors.log')
                    continue

                

            except mysql.connector.Error:
                writeLogFile("Error getting fee status.",'sql_errors.log')
                continue

    except mysql.connector.Error:
        cursor.close()
        db.close()
        writeLogFile("FATAL ERROR reading from farmer_tokens table.",'sql_errors.log')
        return

    try:
        sql = "SELECT * FROM users"
        cursor.execute(sql)
        all_users = cursor.fetchall()

        for row in all_users:
            username = row[Users.username.value]
            user_bal = row[Users.balance.value]
            fee_stat = row[Users.fee_status.value]
            reward_addr = row[Users.reward_addr.value]
            threshold = row[Users.auto_payout.value]
            gas = row[Users.gas.value]

            if user_bal >= threshold:
                bal_minus_gas = float(user_bal - gas)
                if fee_stat == "user":    payment =  bal_minus_gas * Fees.user.value
                elif fee_stat == "beta":  payment =  bal_minus_gas * Fees.beta.value
                elif fee_stat == "alpha": payment =  bal_minus_gas * Fees.alpha.value
                else: 
                    writeLogFile("Invalid fee status for user " + username + ".",'sql_errors.log')
                    continue

                transaction = subprocess.run(["/home/chia/chia-blockchain/venv/bin/chia", "wallet", "send", "-i", "1", "-a", str(payment), "-m", str(gas), "-t", str(reward_addr)], capture_output=True)

                if "error" not in transaction.stdout:
                    # log to CloudWatch once successful
                    writeLogFile(transaction.stdout,'txn.log')
                    
                    try:
                        sql = "UPDATE users SET balance = 0 WHERE user_name = %s"
                        val = (username,)
                        cursor.execute(sql, val)
                    except mysql.connector.Error:
                        writeLogFile("Error setting balance to 0 for user " + username + ".",'sql_errors.log')
                        continue

                    try:
                        sql = "SELECT balance FROM users WHERE user_name = 'supermineroot'"
                        cursor.execute(sql)
                        result = cursor.fetchone()
                        rootbal = float(result[0])
                    except mysql.connector.Error:
                        writeLogFile("Error setting balance to 0 for user " + username + ".",'sql_errors.log','sql_errors.log')
                        continue

                    if   fee_stat == "user":    newrootbal = rootbal + (payment - (payment * Fees.user.value))
                    elif fee_stat == "beta":  newrootbal = rootbal + (payment - (payment * Fees.beta.value))
                    elif fee_stat == "alpha": newrootbal = rootbal + (payment - (payment * Fees.alpha.value))
                    else: 
                        writeLogFile("Invalid fee status for user" + username + "who we just paid.",'sql_errors.log')
                        continue
                    try:
                        sql = "UPDATE users SET balance = %s WHERE user_name = 'supermineroot'"
                        val = (newrootbal,)
                        cursor.execute(sql, val)
                    except mysql.connector.Error:
                        writeLogFile("FATAL ERROR setting balance of " + newrootbal + "for supermineroot.",'sql_errors.log')

                else:
                    writeLogFile(transaction.stdout,'txn_errors.log')




    except mysql.connector.Error:
        cursor.close()
        db.close()
        writeLogFile("FATAL ERROR: could not get * from users table.",'sql_errors.log')
        return

    db.commit()
    cursor.close()
    db.close()

def get_secret(secret_id):
    secret_name = secret_id
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        writeLogFile(e,'secretsmanager_errors.log')
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            return get_secret_value_response['SecretString']
        else:
            return get_secret_value_response['SecretBinary']

        # Your code goes here.
    
if __name__ == "__main__":
    syncPayouts()
