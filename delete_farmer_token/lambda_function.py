import mysql.connector
import os

from mysql.connector.connection import MySQLConnection
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["farmer_token"] is None or event["login_token"] is None:
        raise LambdaException(404)
    
    token = event["farmer_token"]
    session = event["login_token"]

    username = ""
    used_username = ""

    try:
        sql = "SELECT user_name FROM sessions WHERE login_token = %s"
        val = (session,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    try:
        sql = "SELECT user_name FROM farmer_tokens WHERE farmer_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        used_username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    if username != used_username:
        raise LambdaException("400")

    try:
        sql = "DELETE FROM drives WHERE farmer_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        sql = "DELETE FROM memos WHERE farmer_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        sql = "DELETE FROM farmer_tokens WHERE farmer_token = %s"
        val = (token,)
        cursor.execute(sql, val)
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    db.commit()

    cursor.close()
    db.close()

    return {
        'body': {
          "message": "Deleted token."
        }
    }