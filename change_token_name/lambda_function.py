import mysql.connector
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    farmer_token = event["farmer_token"]
    name = event["new_name"]
    login_token = event["login_token"]

    try: 
        sql = "SELECT * FROM users WHERE login_token = %s"
        val = (login_token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        if result is None:
            raise LambdaException(404)
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(400)

    try:
        sql = "UPDATE farmer_tokens SET name = %s WHERE farmer_token = %s"
        val = (name, farmer_token)
        cursor.execute(sql, val)
        cursor.close()
        db.close()
        return
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(400)