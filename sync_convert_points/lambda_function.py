import mysql.connector
from lambda_cache import secrets_manager
from enum import Enum

class LambdaException(Exception):
    pass

class Fees(Enum):
    user = 0.98 # user
    beta = 0.99 # beta tester
    alpha = 1.00 # alpha tester

class farmer_tokens(Enum):
    farmer_token    = 0
    xch_next_ppm    = 1
    user_name       = 2
    xch_points      = 3
    time_last_check = 4
    status          = 5
    xch_fingerprint = 6
    name            = 7
    xfx_next_ppm    = 8
    xfx_fingerprint = 9
    xfx_points      = 10

class Users(Enum):
    user_name        = 0
    password         = 1
    xch_balance      = 2
    xch_fee_status   = 3
    xch_reward_addr  = 4
    xch_auto_payout  = 5
    banned           = 6
    gas              = 7
    xch_lifetime_bal = 8
    xfx_balance      = 9
    xfx_fee_status   = 10
    xfx_reward_addr  = 11 
    xfx_auto_payout  = 12
    xfx_lifetime_bal = 13

class xch_wallets(Enum):
    wallet_address    = 0
    description       = 1
    earned_balance    = 2
    pending_balance   = 3
    height_last_check = 4

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):

    block_height = event['block_height']
    # TODO: add multichain support

    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')
    
    db = mysql.connector.connect(
        host="farmer-database.cydm5y3aznbz.us-east-1.rds.amazonaws.com",
        user=rds_username,
        password=rds_password,
        database="xch_farmer_database",
        port=5400
    )
    cursor = db.cursor()

    lastbal = None
    totalpoints = 0
    bal = 0.0
    addedbal = 0.0
    earned_balance = 0
    xch_wallet_address = "xch1987ezasn8ypge40ycg77a3dxdk4ltp9dpmjyc5sx0pekv4aymdyqlephaz"

    try:
        sql = "SELECT * FROM xch_wallets WHERE xch_wallet_address=%s"
        val = (xch_wallet_address,)
        cursor.execute(sql,val)
        result = cursor.fetchall()
        earned_balance = float(result[0][xch_wallets.earned_balance.value])
        reward_amt = earned_balance

    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("Failed to get XCH wallets.", sql_errors.log)

    try:
        sql = "SELECT SUM(xch_points) FROM farmer_tokens"
        cursor.execute(sql)
        result = cursor.fetchone()

        totalpoints = float(result[0])
    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("Failed to sum points.", sql_errors.log)

    rewardperpoint = float(earned_balance / totalpoints)

    expectedfarmercount = 0
    truefarmercount = 0

    try:
        sql = "SELECT COUNT(*) FROM farmer_tokens"
        cursor.execute(sql)
        result = cursor.fetchone()
        expectedfarmercount = result[0]

    except mysql.connector.Error:
        print("Failed getting farmer count.")
        raise LambdaException(500)

    try:
        sql = "SELECT * FROM farmer_tokens"
        cursor.execute(sql)
        all_farmer_tokens = cursor.fetchall()

        for row in all_farmer_tokens:
            points = row[farmer_tokens.xch_points.value]
            username = row[farmer_tokens.user_name.value]
            farmer_token = row[farmer_tokens.farmer_token.value]

            try:
                sql = "SELECT * FROM users WHERE user_name = %s"
                val = (username,)
                cursor.execute(sql, val)
                result = cursor.fetchone()
                print("105: " + str(result))

                if result is None:
                  continue

                curbal = float(result[Users.xch_balance.value])
                feestatus = result[Users.xch_fee_status.value]

                if feestatus == "user":    addedbal = (points * rewardperpoint) * Fees.user.value
                elif feestatus == "beta":  addedbal = (points * rewardperpoint) * Fees.beta.value
                elif feestatus == "alpha": addedbal = (points * rewardperpoint) * Fees.alpha.value
                else:
                    print("Invalid fee status for user" + username + ".")
                    raise LambdaException(500)

                newbal = (curbal + addedbal)
                earned_balance -= addedbal
                
                if(addedbal > 0):
                    try:
                        sql = "INSERT INTO xch_points_history (farmer_token,percent_points,balance_given,height,reward_amt) VALUES (%s,%s,%s,%s,%s)"
                        val = (farmer_token,float(points/totalpoints)*100,addedbal,block_height,reward_amt)
                        cursor.execute(sql, val)

                    except mysql.connector.Error:
                        print("Failed to add points history for user " + username + " before balance update.")
                        raise LambdaException(500)

                try:
                    sql = "UPDATE users SET xch_balance = %s WHERE user_name = %s"
                    val = (newbal, username)
                    cursor.execute(sql, val)

                except mysql.connector.Error:
                    print("Failed updating balance for user " + username + " before balance update.")
                    raise LambdaException(500)

                try:
                    sql = "UPDATE farmer_tokens SET xch_points = 0 WHERE farmer_token = %s"
                    val = (farmer_token,)
                    cursor.execute(sql, val)

                except mysql.connector.Error:
                    print("Failed updating points for user " + username + " before balance update.")
                    raise LambdaException(500)

                truefarmercount += 1

            except mysql.connector.Error:
                print("Failed getting user table columns for user " + username + " before balance update.")
                raise LambdaException(500)

    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("FATAL ERROR reading all rows from farmer_tokens table.")
        raise LambdaException(500)

    if truefarmercount < expectedfarmercount:
        print("Handle me")

    root_user_balance = 0.0
    root_user_lifetime_bal = 0.0

    if earned_balance < 0:
       cursor.close()
       db.close()
       print("FATAL: earned_balance < 0; Value = " + str(earned_balance))
       raise LambdaException(500) 

    try:
        sql = "SELECT * FROM users where user_name=%s"
        val = ("supermineroot",)
        cursor.execute(sql,val)
        root_user = cursor.fetchone()

        root_user_balance      = float(root_user[Users.xch_balance.value]) + earned_balance
        root_user_lifetime_bal = float(root_user[Users.xch_lifetime_bal.value]) + earned_balance

    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("FATAL ERROR reading supermineroot user from users table.")
        raise LambdaException(500)

    if(earned_balance > 0):
        try:
            sql = "INSERT INTO xch_points_history (farmer_token,percent_points,balance_given,height,reward_amt) VALUES (%s,%s,%s,%s,%s)"
            val = ("supermineroot",0,earned_balance,block_height,reward_amt)
            cursor.execute(sql, val)

        except mysql.connector.Error:
            print("Failed to add points history for user " + username + " before balance update.")
            raise LambdaException(500)

    try:
        sql = "UPDATE users SET xch_balance=%s,xch_lifetime_bal=%s WHERE user_name=%s"
        val = (root_user_balance, root_user_lifetime_bal, "supermineroot")
        cursor.execute(sql,val)
        root_user = cursor.fetchone()

    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("FATAL ERROR updating balance for supermineroot user.")
        raise LambdaException(500)

    try:
        sql = "UPDATE xch_wallets SET earned_balance=%s where xch_wallet_address=%s"
        val = (0, xch_wallet_address)
        cursor.execute(sql,val)

    except mysql.connector.Error:
        cursor.close()
        db.close()
        print("FATAL ERROR udpating earned_balance for xch_wallet_address " + xch_wallet_address + ".")
        raise LambdaException(500)

    db.commit()
    cursor.close()
    db.close()

    return {
        'body': {
          'message': 'Points converted to balance successfully.'
        }
    }
