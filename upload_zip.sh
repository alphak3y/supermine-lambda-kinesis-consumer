echo 'Environment:'
env=$1
echo $env

echo 'Folder name:'
folder_name=$2
echo $folder_name

echo 'Lambda name:'
lambda_name=$3
echo $lambda_name

timestamp=`date +%s`

cd ${folder_name}
pip install -r ./requirements.txt --target ./requirements
cd requirements
zip -r ../${folder_name}_${timestamp}.zip .
cd ..
zip -g ${folder_name}_${timestamp}.zip lambda_function.py

aws lambda update-function-code --function-name ${lambda_name} --zip-file fileb://${folder_name}_${timestamp}.zip
rm *.zip
cd ..
