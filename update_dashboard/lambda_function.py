import mysql.connector
from enum import Enum
import os
from lambda_cache import secrets_manager
import datetime

class LambdaException(Exception):
    pass

class Users(Enum):
    user_name        = 0
    password         = 1
    xch_balance      = 2
    xch_fee_status   = 3
    xch_reward_addr  = 4
    xch_auto_payout  = 5
    banned           = 6
    gas              = 7
    xch_lifetime_bal = 8
    hdd_balance      = 9
    hdd_fee_status   = 10
    hdd_reward_addr  = 11 
    hdd_auto_payout  = 12
    hdd_lifetime_bal = 13

class farmer_tokens(Enum):
    farmer_token    = 0
    xch_next_ppm    = 1
    user_name       = 2
    xch_points      = 3
    time_last_check = 4
    status          = 5
    xch_fingerprint = 6
    name            = 7
    hdd_next_ppm    = 8
    hdd_fingerprint = 9
    hdd_points      = 10

class xch_pool_stats(Enum):
    netspace_k32    = 0
    mstimestamp     = 1
    fee             = 2
    min_pay         = 3
    capacity_k32    = 4
    num_farmers     = 5
    xch_price_usd   = 6
    block_height    = 7

class pool_stats(Enum):
    netspace = 0
    pool_space = 1
    mstimestamp = 2

def string_byte_count(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    token = event["login_token"]
    if token is None:
        raise LambdaException("400")

    result = None

    response = {}
    response['xch'] = {}
    farmers = {}

    username = ""

    try:
        sql = "SELECT user_name FROM sessions WHERE login_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    try:
        sql = "SELECT SUM(xch_points) FROM farmer_tokens"
        cursor.execute(sql,)
        result = cursor.fetchone()
        xch_pool_total_points = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    try:
        sql = "SELECT * FROM users WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        user_stats = cursor.fetchone()
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    try:
        sql = "SELECT * FROM xch_pool_stats"
        cursor.execute(sql,)
        xch_stats = cursor.fetchone()
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("500")

    xch_netspace_k32   = float(xch_stats[xch_pool_stats.netspace_k32.value])
    xch_netspace_bytes = xch_netspace_k32 * 108877000000

    xch_capacity_bytes = float(xch_stats[xch_pool_stats.capacity_k32.value]) * 108877000000
    xch_price_usd      = float(xch_stats[xch_pool_stats.xch_price_usd.value])

    response["xch"]["xch_price_usd"]      = xch_price_usd
    response["xch"]["total_netspace"]     = string_byte_count(xch_netspace_bytes)
    response["xch"]["total_pooled_space"] = string_byte_count(xch_capacity_bytes)
    response["xch"]["number_of_farmers"]  = xch_stats[xch_pool_stats.num_farmers.value]

    xch_total_points = 0
    hdd_total_points = 0
    xch_total_ppm = 0
    hdd_total_ppm = 0
    #SELECT time_last_active for each farmer_token
    time_last_active = ""
    total_points = {}
    total_ppm = {}

    response["xch"]["pending_balance"]  = user_stats[Users.xch_balance.value]
    response["xch"]["lifetime_balance"] = user_stats[Users.xch_lifetime_bal.value]

    try:
        sql = "SELECT * FROM farmer_tokens WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    for farmer_token in result:
        token = {}
        xch_total_points += farmer_token[farmer_tokens.xch_points.value]
        xch_total_ppm += farmer_token[farmer_tokens.xch_next_ppm.value]

        time_last_check = farmer_token[farmer_tokens.time_last_check.value]
        if time_last_check is not None:
            time_last_active = time_last_check / 1000
            time = datetime.datetime.fromtimestamp(time_last_active).strftime('%Y-%m-%d %H:%M:%S.%f')
            token["last_update"] = time
            token["xch_plot_count"] = farmer_token[farmer_tokens.xch_next_ppm.value]
            token["name"] = farmer_token[farmer_tokens.name.value]
        else:
            token["last_update"] = "Never Started."
            token["plot_count"] = "No plots."
            token["name"] = farmer_token[farmer_tokens.name.value]
        farmers[farmer_token[farmer_tokens.farmer_token.value]] = token

    response["xch"]["expected_xch_earnings_next_block"] = float(xch_total_points) / float(xch_pool_total_points)*2
    response["xch"]["your_pooled_plot_space"] = string_byte_count(xch_total_ppm * 108877000000)
    response["farmers"] = farmers

    xch_per_day = float((xch_total_ppm / xch_netspace_k32) * 9216)

    response["xch"]["xch_per_day"]   = xch_per_day
    response["xch"]["xch_per_week"]  = xch_per_day * 7
    response["xch"]["xch_per_month"] = xch_per_day * 30

    response["xch"]["usd_per_day"]   = xch_per_day * xch_price_usd
    response["xch"]["usd_per_week"]  = xch_per_day * xch_price_usd * 7
    response["xch"]["usd_per_month"] = xch_per_day * xch_price_usd * 30

    return response 