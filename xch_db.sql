CREATE DATABASE IF NOT EXISTS xch_farmer_database;

USE xch_farmer_database;


CREATE TABLE IF NOT EXISTS users
(
    user_name           VARCHAR(256) NOT NULL,
    password            VARCHAR(256) NOT NULL,
    xch_balance         DECIMAL(25, 12) DEFAULT 0.0,
    hdd_balance         DECIMAL(25, 12) DEFAULT 0.0,
    xch_fee_status      VARCHAR(32) DEFAULT 'user',
    hdd_fee_status      VARCHAR(32) DEFAULT 'user',
    xch_reward_addr     VARCHAR(128) NOT NULL,
    hdd_reward_addr     VARCHAR(128) NOT NULL,
    xch_auto_payout     DECIMAL(6, 4) DEFAULT 0.05,
    hdd_auto_payout     DECIMAL(6, 4) DEFAULT 0.05,
    banned              BOOLEAN,
    gas                 DECIMAL(25, 12) DEFAULT 0.0,
    xch_lifetime_bal    DECIMAL(25, 12) DEFAULT 0.0,
    hdd_lifetime_bal    DECIMAL(25, 12) DEFAULT 0.0,
    PRIMARY KEY         (user_name)
);

ALTER TABLE users RENAME COLUMN balance TO xch_balance;
ALTER TABLE users RENAME COLUMN fee_status TO xch_fee_status;
ALTER TABLE users RENAME COLUMN reward_addr TO xch_reward_addr;
ALTER TABLE users RENAME COLUMN auto_payout TO xch_auto_payout;
ALTER TABLE users RENAME COLUMN lifetime_bal TO xch_lifetime_bal;

ALTER TABLE users ADD hdd_balance DECIMAL(25, 12) DEFAULT 0.0;
ALTER TABLE users ADD hdd_fee_status VARCHAR(32) DEFAULT 'user';
ALTER TABLE users ADD hdd_reward_addr VARCHAR(128) NOT NULL;
ALTER TABLE users ADD hdd_auto_payout DECIMAL(6, 4) DEFAULT 0.05;
ALTER TABLE users ADD hdd_lifetime_bal DECIMAL(25, 12) DEFAULT 0.0;

CREATE TABLE IF NOT EXISTS sessions
(
    login_token     VARCHAR(512),
    user_name       VARCHAR(256),
    PRIMARY KEY     (login_token)
);

CREATE TABLE IF NOT EXISTS farmer_tokens
(
    farmer_token    VARCHAR(512) NOT NULL,
    xch_next_ppm    INT unsigned DEFAULT 0,
    hdd_next_ppm    INT unsigned DEFAULT 0,
    user_name       VARCHAR(256) NOT NULL,
    xch_points      BIGINT unsigned DEFAULT 0,
    hdd_points      BIGINT unsigned DEFAULT 0,
    time_last_check BIGINT unsigned,
    status          BOOLEAN,
    xch_fingerprint INT unsigned,
    hdd_fingerprint INT unsigned,
    name            VARCHAR(128),
    PRIMARY KEY     (farmer_token)
);

CREATE TABLE IF NOT EXISTS xch_remote_harvester
(
    remote_harvester    VARCHAR(512) NOT NULL,
    owned_plots         INT unsigned NOT NULL,
    PRIMARY KEY         (remote_harvester)
);

CREATE TABLE IF NOT EXISTS hdd_remote_harvester
(
    remote_harvester    VARCHAR(512) NOT NULL,
    owned_plots         INT unsigned NOT NULL,
    PRIMARY KEY         (remote_harvester)
);

ALTER TABLE farmer_tokens RENAME COLUMN next_ppm TO xch_next_ppm;
ALTER TABLE farmer_tokens RENAME COLUMN points TO xch_points;
ALTER TABLE farmer_tokens RENAME COLUMN fingerprint TO xch_fingerprint;

ALTER TABLE farmer_tokens ADD hdd_next_ppm INT unsigned DEFAULT 0;
ALTER TABLE farmer_tokens ADD hdd_fingerprint INT unsigned;
ALTER TABLE farmer_tokens ADD hdd_points BIGINT unsigned DEFAULT 0;

INSERT IGNORE INTO users (user_name, password, reward_addr, fee_status) VALUES ("leo","fMqgXvBRcRSs7QYCFN5F9v3np", "xch123456789", "alpha");

CREATE TABLE IF NOT EXISTS xch_plot_ids
(
    plot_id             VARCHAR(512) NOT NULL,
    farmer_token        VARCHAR(512) NOT NULL,
    remote_harvester    VARCHAR(512) NOT NULL,
    time_last_check     BIGINT unsigned,
    PRIMARY KEY         (plot_id)
);

CREATE TABLE IF NOT EXISTS hdd_plot_ids
(
    plot_id             VARCHAR(512) NOT NULL,
    farmer_token        VARCHAR(512) NOT NULL,
    remote_harvester    VARCHAR(512) NOT NULL,
    time_last_check     BIGINT unsigned,
    PRIMARY KEY         (plot_id)
);

CREATE TABLE IF NOT EXISTS blacklist
(
    plot_id                 VARCHAR(512) NOT NULL,
    xch_remote_harvester    VARCHAR(512) NOT NULL,
    hdd_remote_harvester    VARCHAR(512) NOT NULL,
    xch_fingerprint         INT unsigned,
    hdd_fingerprint         INT unsigned,
    PRIMARY KEY             (plot_id)
);

CREATE TABLE IF NOT EXISTS xch_pool_stats
(
    netspace_k32            DECIMAL(30,0) DEFAULT 0.0,
    mstimestamp             BIGINT unsigned,
    fee                     DECIMAL(6, 4) DEFAULT 2,
    min_pay                 DECIMAL(6, 4) DEFAULT 0.05,
    capacity_k32            BIGINT unsigned DEFAULT 0,
    num_farmers             INT unsigned DEFAULT 0,
    xch_price_usd           DECIMAL(20,2) DEFAULT 0.00,
    PRIMARY KEY             (mstimestamp)
);

CREATE TABLE IF NOT EXISTS xch_farmed_blocks
(                 
    xch_amt                 DECIMAL(14,12),
    height                  BIGINT unsigned,
    txn_id                  VARCHAR(512) NOT NULL,
    mstimestamp             BIGINT unsigned,
    PRIMARY KEY             (txn_id)
);

CREATE TABLE IF NOT EXISTS xch_wallets
(
    xch_wallet_address      VARCHAR(512) NOT NULL,
    description             VARCHAR(512) NOT NULL,
    earned_balance          DECIMAL(25, 12) DEFAULT 0.0,
    pending_balance         DECIMAL(25, 12) DEFAULT 0.0,
    height_last_check       BIGINT unsigned DEFAULT 0,
    PRIMARY KEY             (xch_wallet_address)
);

CREATE TABLE IF NOT EXISTS xch_points_history
(
    farmer_token            VARCHAR(512) NOT NULL,
    percent_points          DECIMAL(7, 4) NOT NULL, 
    balance_given           DECIMAL(14, 12) NOT NULL,
    height                  BIGINT unsigned NOT NULL,
    reward_amt              DECIMAL(14, 12) NOT NULL 
);

CREATE TABLE IF NOT EXISTS xch_payout_txns
(
    txn_id                  VARCHAR(512) NOT NULL,
    farmer_token            VARCHAR(512) NOT NULL,
    xch_amt                 DECIMAL(14, 12) NOT NULL,
    PRIMARY KEY             (txn_id)
);

CREATE TABLE IF NOT EXISTS hdd_pool_stats
(
    netspace_k32            DECIMAL(30,0) DEFAULT 0.0,
    mstimestamp             BIGINT unsigned,
    fee                     DECIMAL(6, 4) DEFAULT 2,
    min_pay                 DECIMAL(6, 4) DEFAULT 0.05,
    capacity_k32            BIGINT unsigned DEFAULT 0,
    num_farmers             INT unsigned DEFAULT 0,
    hdd_price_usd           DECIMAL(20,2) DEFAULT 0.00,
    PRIMARY KEY             (mstimestamp)
);

CREATE TABLE IF NOT EXISTS hdd_farmed_blocks
(                 
    hdd_amt                 DECIMAL(14,12),
    height                  BIGINT unsigned,
    txn_id                  VARCHAR(512) NOT NULL,
    mstimestamp             BIGINT unsigned,
    PRIMARY KEY             (txn_id)
);

CREATE TABLE IF NOT EXISTS hdd_wallets
(
    hdd_wallet_address      VARCHAR(512) NOT NULL,
    description             VARCHAR(512) NOT NULL,
    earned_balance          DECIMAL(25, 12) DEFAULT 0.0,
    pending_balance         DECIMAL(25, 12) DEFAULT 0.0,
    height_last_check       BIGINT unsigned DEFAULT 0,
    PRIMARY KEY             (hdd_wallet_address)
);

CREATE TABLE IF NOT EXISTS hdd_payout_txns
(
    txn_id                  VARCHAR(512) NOT NULL,
    farmer_token            VARCHAR(512) NOT NULL,
    hdd_amt                 DECIMAL(14, 12) NOT NULL,
    PRIMARY KEY             (txn_id)
);

INSERT IGNORE INTO xch_wallets (xch_wallet_address, description) VALUES ("xch1987ezasn8ypge40ycg77a3dxdk4ltp9dpmjyc5sx0pekv4aymdyqlephaz","OG Pool Wallet #1");