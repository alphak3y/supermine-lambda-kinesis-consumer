import json
from types import LambdaType
import mysql.connector
import os
from lambda_cache import secrets_manager
from enum import Enum
import sys
import datetime
import requests

class LambdaException(Exception):
    pass

class xch_wallets(Enum):
    wallet_address    = 0
    description       = 1
    earned_balance    = 2
    pending_balance   = 3
    height_last_check = 4

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):

    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    wallets = []
    
    try:
        sql = "SELECT * FROM xch_wallets"
        cursor.execute(sql,)
        wallets = cursor.fetchall()
    except mysql.connector.Error as error:
        print(error)
        cursor.close()
        db.close()
        return

    for wallet in wallets:

        offset = 0
        reward_txns = []
        farmed_blocks = []

        wallet_address        = wallet[xch_wallets.wallet_address.value]
        earned_balance        = int(wallet[xch_wallets.earned_balance.value])
        height_last_check     = int(wallet[xch_wallets.height_last_check.value])
        new_height_last_check = int(wallet[xch_wallets.height_last_check.value])

        in_sync = False

        while(in_sync == False):

            reward_txns = get_reward_txns(wallet_address, 25, offset, height_last_check)

            if(reward_txns == []):
              break

            if(offset == 0):
               new_height_last_check = reward_txns[0]['block']['height'] 

            for txn in reward_txns:
              xch_amount = int(txn['amount']) / 10**12         
              insert_farmed_block(txn, xch_amount, cursor)
              
              earned_balance += xch_amount

              if txn['block']['height'] <= height_last_check:
                in_sync = True
                break

            offset += 25

        try:
            sql = "UPDATE xch_wallets SET height_last_check = %s, earned_balance = %s WHERE xch_wallet_address = %s"
            val = (new_height_last_check, earned_balance, wallet_address)
            cursor.execute(sql, val)
            wallets = cursor.fetchall()
        except mysql.connector.Error as error:
            print(error)
            cursor.close()
            db.close()
            continue

    db.commit()
    cursor.close()
    db.close()

def get_reward_txns(wallet_addr, num_txns, txn_offset, height_last_check):

    req = "https://xchscan.com/api/account/txns"
    addr = "?address="
    limit = "&limit="
    offset = "&offset="

    request_url = req + addr + wallet_addr + limit + str(num_txns) + offset + str(txn_offset)
    print(request_url)

    response = requests.get(request_url)
    response_json = json.loads(response.text)

    txns = response_json['txns']
    reward_txns = []

    for txn in txns:
        if txn['from'] is None and txn['block']['height'] > height_last_check:
            reward_txns.append(txn)
        elif txn['block']['height'] <= height_last_check:
            break


    print('found farming rewards: ')
    print(json.dumps(reward_txns,indent=2))

    return reward_txns

def insert_farmed_block(txn, xch_amount, cursor):
    try:
        sql = "INSERT INTO xch_farmed_blocks (xch_amt, height, txn_id, mstimestamp) VALUES (%s, %s, %s, %s)"
        val = (xch_amount, txn['block']['height'], txn['id'], txn['timestamp'])
        cursor.execute(sql,val)
    except mysql.connector.Error as error:
        print(error)
        cursor.close()
        db.close()
        return

