import mysql.connector
import time
import sslcrypto
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass


@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["username"] is None or event["password"] is None:
        raise LambdaException("404")

    username = event["username"]
    password = event["password"]

    result = None

    try:
    # check hashed/encrypted password matches stored hashed/encrypted password
        sql = "SELECT password FROM users where user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        cursor.close()
        db.close()
    except mysql.connector.Error:
        cursor.close()
        db.close()

    if result is None:
        raise LambdaException("404")

    validatorPassword = result[0]

    if validatorPassword == password:
        logintoken = generateLoginToken(username,rds_username,rds_password)
        return {
            'body': {
              "login_token": logintoken
            }
        }
    else:
      raise LambdaException("404")

def generateLoginToken(username,rds_username,rds_password):
    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()


    public_key = b'\x033\xc7\xdfVH1\x03\xf6K+\x9d2\xac$6\xcb\xbfN\xe7Oi\x8cM\xe7\xee\xb4\xe5a\xd4*\x80U'

    rightnow = str(round(time.time() * 1000))
    hash = rightnow + username

    # encode
    curve = sslcrypto.ecc.get_curve("secp256k1")
    ciphertext = curve.encrypt(hash, public_key, algo="aes-256-ctr")

    token = ciphertext.hex()


    sql = "INSERT INTO sessions (login_token, user_name) VALUES(%s, %s)"
    val = (token, username)
    cursor.execute(sql, val)

    db.commit()

    cursor.close()
    db.close()

    return token

