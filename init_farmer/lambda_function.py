import mysql.connector
import time
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["farmer_token"] is None or event["fingerprint"] is None or event["plot_count"] is None: #or event["reward_addr_changed"] is None:
        raise LambdaException(404)

    #print(event)

    token = event["farmer_token"]
    fingerprints = event["fingerprint"]
    chainlist = event["plot_count"]
    #reward_addr_changed = event["reward_addr_changed"]


    rightnow = round(time.time() * 1000)

    try:
        sql = "SELECT time_last_check FROM farmer_tokens WHERE farmer_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()

        if result is not None:
          tlc = result[0]
          if tlc is not None and (rightnow - tlc) < 180000:
            print("Farmer token " + token + "is sending too many INIT requests.")
            raise LambdaException(500)
        else:
           raise LambdaException(500)
           
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(500)

    """
    if reward_addr_changed is True:
        try:
            sql = "UPDATE users SET banned = %s WHERE farmer_token = %s"
            val = (reward_addr_changed, token)
            cursor.execute(sql, val)
        except mysql.connector.Error:
            cursor.close()
            db.close()
            raise LambdaException(404)
    """

    for network in fingerprints:
        network_clmn = str(network) + "_fingerprint"
        for fingerprint in fingerprints[network]:
            #TODO: update fingerprint to be its own table so a single farmer_token can have multiple fingerprints on multiple networks
            try:
                sql = "UPDATE farmer_tokens SET " + network_clmn + " = %s WHERE farmer_token = %s"
                val = (fingerprint, token)
                cursor.execute(sql, val)
            except mysql.connector.Error:
                cursor.close()
                db.close()
                raise LambdaException(500)

    # calculate next ppm
    for chain in chainlist:
        chainppm = 0
        for harvester in chainlist[chain]:
            amt = 0
            #print(harvester)
            for size in chainlist[chain][harvester]:
                amt += int(chainlist[chain][harvester][size])
                size = int(size)

                if size >= 32 and size <= 35:
                    points = 2 ** (size - 32)
                else:
                    #eprint("Plot size invalid! k: " + str(size) + " amt: " + str(amt) + " farmer_token: " + str(token))
                    raise LambdaException(500)
                chainppm += (amt * points)

            remote_table = chain + "_remote_harvester"
            try:
                sql = "INSERT INTO " + remote_table + " (remote_harvester, owned_plots) VALUES(%s, %s)"
                val = (harvester, amt)
                cursor.execute(sql, val)
            except mysql.connector.Error as error:
                try:
                    sql = "UPDATE " + remote_table + " SET owned_plots = %s WHERE remote_harvester = %s"
                    val = (amt, harvester)
                    cursor.execute(sql, val)
                except mysql.connector.Error as error:
                    #eprint(error)
                    cursor.close()
                    db.close()
                    raise LambdaException(500)

        next_ppm_chain = chain + "_next_ppm"
        try:
            sql = "UPDATE farmer_tokens SET " + next_ppm_chain + " = %s WHERE farmer_token = %s"
            val = (chainppm, token)
            cursor.execute(sql, val)
        except mysql.connector.Error as error:
            #eprint(error)
            cursor.close()
            db.close()
            raise LambdaException(500)

    # creates timestamp
    try:
        sql = "UPDATE farmer_tokens SET time_last_check = %s WHERE farmer_token = %s"
        val = (rightnow, token)
        cursor.execute(sql, val)
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(500)


    db.commit()

    cursor.close()
    db.close()


    return {
        'body': {
          'message': 'Client initialized successfully.'
        }
    }