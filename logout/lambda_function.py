import mysql.connector
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass


@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    token = event["login_token"]
    action = event["action"]

    if token is None:
        raise LambdaException("400")

    if action == "all":
        username = ""
        try:
            sql = "SELECT user_name FROM sessions WHERE login_token = %s"
            val = (token,)
            cursor.execute(sql, val)
            result = cursor.fetchone()
            username = result[0]
        except mysql.connector.Error:
            cursor.close()
            db.close()
            raise LambdaException("404")
        try:
            sql = "DELETE FROM sessions WHERE user_name = %s"
            val = (username,)
            cursor.execute(sql, val)
            cursor.close()
            db.close()
            return {
                "body": {
                    "message": "Logged out"
                }
            }
        except mysql.connector.Error:
            cursor.close()
            db.close()
            raise LambdaException("404")

    elif action == "current":
        try:
            sql = "DELETE FROM sessions WHERE login_token = %s"
            val = (token,)
            cursor.execute(sql, val)
            cursor.close()
            db.close()
            return {
                "body": {
                    "message": "Logged out"
                }
            }
        except mysql.connector.Error:
            cursor.close()
            db.close()
            raise LambdaException("404")

    else:
        return {
            "body": {
                "message": "Invalid action"
            }
        }