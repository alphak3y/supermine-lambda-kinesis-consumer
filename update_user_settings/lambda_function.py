import mysql.connector
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )

    cursor = db.cursor()

    token = event["token"]

    new_username = event["new_username"]
    new_xch_reward_addr = event["new_xch_reward_addr"]
    new_xch_threshold = event["new_xch_threshold"]
    new_xfx_reward_addr = event["new_xfx_reward_addr"]
    new_xfx_threshold = event["new_xfx_threshold"]

    new_list = {}
    new_list["user_name"] = new_username
    new_list["xch_reward_addr"] = new_xch_reward_addr
    new_list["xch_auto_payout"] = new_xch_threshold
    new_list["xfx_reward_addr"] = new_xfx_reward_addr
    new_list["xfx_auto_payout"] = new_xfx_threshold

    if token is None:
        raise LambdaException("400")

    username = ""

    try:
        sql = "SELECT user_name FROM sessions WHERE login_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("304")

    for key in new_list:
        try:
            sql = "UPDATE users SET " + key + " = %s WHERE user_name = %s"
            val = (new_list[key], username)
            cursor.execute(sql, val)
        except mysql.connector.Error:
            cursor.close()
            db.close()
            raise LambdaException("304")

    db.commit()
    cursor.close()
    db.close()

    return {
        "body": "updated ok"
    }