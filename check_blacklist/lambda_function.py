import mysql.connector
import os
import boto3
from botocore.exceptions import ClientError
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):

    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    fingerprint = event["fingerprint"]

    try:
    #TODO maybe have greylist?
    # where we store possible cheaters to be manually checked?
        sql = "SELECT banned FROM users WHERE fingerprint = %s"
        val = (fingerprint,)
        cursor.execute(sql, val)
        result = cursor.fetchone()

        if result is None:
            print("passed")
            return {
                'body': {
                  'message': 'Blacklist check passed.'
                }
            }

        else:
            print("user is banned")
            raise LambdaException(302)

    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(500)
