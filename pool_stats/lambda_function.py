import json
from types import LambdaType
import mysql.connector
import os
from lambda_cache import secrets_manager
from enum import Enum
import sys
import time
import datetime
import requests

class LambdaException(Exception):
    pass

class xch_pool_stats(Enum):
    netspace_k32    = 0
    mstimestamp     = 1
    fee             = 2
    min_pay         = 3
    capacity_k32    = 4
    num_farmers     = 5
    xch_price_usd   = 6
    block_height    = 7

class xch_farmed_blocks(Enum):
    xch_amt      = 0
    height       = 1
    txn_id       = 2
    mstimestamp  = 3

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def string_byte_count(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    netspace_k32          = 0 # hit some API in update
    mstimestamp_now       = int(round(time.time() * 1000))
    # only include farmers active in the last 24 hours
    mstimestamp_yesterday = int(round(time.time() * 1000)) - 86400000
    capacity_bytes        = 0 # add up points to calculate space
    capacity_k32          = 0
    farmed_blocks         = []
    xch_price_usd         = 0.00

    # get pool stats
    try:
        sql = "SELECT * FROM xch_pool_stats"
        cursor.execute(sql,)
        result = cursor.fetchone()
        print(result)
        netspace_k32   = result[xch_pool_stats.netspace_k32.value]
        capacity_k32
        netspace_bytes = float(result[xch_pool_stats.netspace_k32.value]) * 108877000000
        capacity_bytes = float(result[xch_pool_stats.capacity_k32.value]) * 108877000000
        capacity_k32   = result[xch_pool_stats.capacity_k32.value]
        num_farmers    = result[xch_pool_stats.num_farmers.value]
        fee            = result[xch_pool_stats.fee.value]
        min_pay        = result[xch_pool_stats.min_pay.value]
        xch_price_usd  = float(result[xch_pool_stats.xch_price_usd.value])
        block_height   = result[xch_pool_stats.block_height.value]

    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    try:
        sql = "SELECT * FROM xch_farmed_blocks WHERE xch_amt=1.75 ORDER BY height DESC LIMIT 5"
        cursor.execute(sql,)
        farmed_blocks_tuples = cursor.fetchall()

    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    for block_tuple in farmed_blocks_tuples:
        farmed_block = {}
        farmed_block['farmer'] = 'xch1987ezasn8ypge40ycg77a3dxdk4ltp9dpmjyc5sx0pekv4aymdyqlephaz'
        farmed_block['height'] = int(block_tuple[xch_farmed_blocks.height.value])
        farmed_block['number'] = int(block_tuple[xch_farmed_blocks.height.value])
        farmed_block['time']   = int(block_tuple[xch_farmed_blocks.mstimestamp.value])
        farmed_block['amount'] = float(block_tuple[xch_farmed_blocks.xch_amt.value])
        farmed_block['txnId']  = block_tuple[xch_farmed_blocks.txn_id.value]

        farmed_blocks.append(farmed_block)
     
    last_block_time = datetime.datetime.fromtimestamp(farmed_blocks[0]['time']).strftime('%Y-%m-%d %H:%M:%S.%f')
    netspace_percent = float(capacity_k32/netspace_k32)*100
    expected_time_to_win = 100/(netspace_percent) * 18.75 / 3600
    expected_time_to_win_str = ""

    if expected_time_to_win > 24:
        expected_time_to_win_str = str(expected_time_to_win/24) + ' Days'
    elif expected_time_to_win > 1:
        expected_time_to_win_str = str(expected_time_to_win) + ' Hours'
    else:
        expected_time_to_win_str = str(expected_time_to_win*60) + ' Minutes'

    response = {
        "statusCode": 200,
        "headers": {
          "Content-Type": "application/json"
         },
        "body": {
          "status": "ok",
          "poolInfo": {
            "fee":     float(fee),
            "feeType": "PPLNS",
            "minPay":  float(min_pay),
            "title":   "SuperMine",
            "url":     "https://supermine.io",
            "nodes":   []
          },
          "farmers":        num_farmers,
          "capacityBytes":  capacity_bytes,
          "netspaceString": string_byte_count(netspace_bytes),
          "capacityString": string_byte_count(capacity_bytes),
          "netspacePercent": netspace_percent,
          "farmedBlocks":   farmed_blocks,
          "lastBlockTime" : last_block_time,
          "totalPoolPlots": capacity_k32,
          "expectedTimeToWin": expected_time_to_win_str,
          "xchPriceUsd": f'{xch_price_usd:.2f}',
          "height": block_height
        }
    }

    return response


