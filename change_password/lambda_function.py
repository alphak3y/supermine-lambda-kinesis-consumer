import mysql.connector
#import time
#import sslcrypto
import os
#import boto3
from lambda_cache import secrets_manager


class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["username"] is None or event["old_password"] is None or event["new_password"] is None:
        raise LambdaException("404")

    username = event["username"]
    password = event["old_password"]
    newpassword = event["new_password"]


    try:
        sql = "SELECT password FROM users WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        truepass = result[0]

        #private_key = b"\x11\xffW\x8f\x92;\x86\x89Frw\x06\xc1bv\xbe\x8fo\xdc\xcf\x1e\x19?Q!'P\xb9\xf6\x16\xbc\x00\x01"

        #curve = sslcrypto.ecc.get_curve("secp256k1")

        #truepass = curve.decrypt(bytes.fromhex(truepass), private_key, algo="aes-256-ctr")
        #password = bytes.fromhex(password)
        # truepass is decrypted, password is not

        # if decrypted truepass is not the same as decrypted password then this fails
        #assert curve.decrypt(password, private_key, algo="aes-256-ctr") == truepass

        #public_key = b'\x033\xc7\xdfVH1\x03\xf6K+\x9d2\xac$6\xcb\xbfN\xe7Oi\x8cM\xe7\xee\xb4\xe5a\xd4*\x80U'
        # encrypt newpassword -> newpass
        #ciphertext = curve.encrypt(newpassword, public_key, algo="aes-256-ctr")
        #newpass = ciphertext.hex()

        if truepass == password:

            sql = "UPDATE users SET password = %s WHERE user_name = %s"
            val = (newpassword, username)
            cursor.execute(sql, val)


            db.commit()
            cursor.close()
            db.close()

            return {
                'body': {
                  'message': "Password changed succesfully."
                }
            }
        else:
            raise LambdaException("404")

    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")
