import json
from types import LambdaType
import mysql.connector
import os
from lambda_cache import secrets_manager
from enum import Enum
import sys
import time
import requests

class LambdaException(Exception):
    pass

class xch_pool_stats(Enum):
    netspace_k32    = 0
    mstimestamp     = 1
    fee             = 2
    min_pay         = 3
    capacity_k32    = 4
    num_farmers     = 5
    xch_price_usd   = 6
    block_height    = 7

# not being used just yet
class xch_blocks_found(Enum):
    farmer_token = 0
    mstimestamp  = 1
    xch_amt      = 2
    height       = 3

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    netspace_bytes        = 0 # hit some API to obtain
    mstimestamp_now       = int(round(time.time() * 1000))
    mstimestamp_yesterday = int(round(time.time() * 1000)) - 86400000
    capacity_bytes        = 0 # add up points to calculate space
    num_farmers           = 0

    req_url = "https://xchscan.com/api/chia-price"
    response = requests.get(req_url)
    response_json = json.loads(response.text)

    xch_price_usd = int(response_json['usd'])

    req_url = "https://xchscan.com/api/netspace"
    response = requests.get(req_url)
    response_json = json.loads(response.text)

    netspace_k32 = int(response_json['netspace'] // 108877000000)

    req_url = "https://xchscan.com/api/blocks?limit=1&offset=0"
    response = requests.get(req_url)
    response_json = json.loads(response.text)

    block_height = int(response_json['blocks'][0]['height'])

    # sum up points to get capacity_bytes
    try:
        sql = "SELECT SUM(next_ppm) FROM farmer_tokens WHERE time_last_check > %s AND time_last_check <= %s"
        val = (mstimestamp_yesterday, mstimestamp_now)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        
        if result[0] == None:
          capacity_k32 = 0
        else:
          capacity_k32 = int(result[0])
            
        capacity_bytes = capacity_k32 * 108877000000


    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    # count farmers active
    try:
        sql = "SELECT COUNT(farmer_token)FROM farmer_tokens WHERE time_last_check > %s AND time_last_check <= %s"
        val = (mstimestamp_yesterday, mstimestamp_now)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        num_farmers = int(result[0]);

    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    # clear table
    try:
        sql = "DELETE FROM xch_pool_stats"
        cursor.execute(sql)

    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    # update pool capacity, timestamp, and num_farmers
    try:
        sql = "INSERT INTO xch_pool_stats (mstimestamp, capacity_k32, num_farmers, xch_price_usd, netspace_k32, block_height) VALUES (%s, %s, %s, %s, %s, %s)"
        print(mstimestamp_now)
        print(capacity_k32)
        val = (mstimestamp_now, capacity_k32, num_farmers, xch_price_usd, netspace_k32, block_height)
        cursor.execute(sql, val)
    except mysql.connector.Error as error:
        print(error)
        raise LambdaException(500)

    print("committing...")
    db.commit()
    cursor.close()
    db.close()
    print("done.")

    return {
        'message': 'Pool stats processed successfully.'
    }


