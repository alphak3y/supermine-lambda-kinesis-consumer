import mysql.connector
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    if event["username"] is None:
        raise LambdaException(404)

    
    username = event["username"]

    # get all the farmer_tokens for user
    # get all the drives for each farmer_token along with fingerprint
    # insert (serial_num, fingerprint) into blacklist table

    try:
        sql = "UPDATE users SET banned = TRUE WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)

        sql = "SELECT fingerprint FROM farmer_tokens WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        results = cursor.fetchall()

        # add each fingerprint from the user into the blacklist
        for result in results:
            fingerprint = ''.join(map(str,result[0]))
            fingerprint = fingerprint[1:len(fingerprint) - 2]



        sql = "SELECT farmer_token FROM farmer_tokens WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        result = cursor.fetchall()

        for x in result:
            farmer_token = ''.join(x)
            try:
                sql = "SELECT serial_num FROM drives WHERE farmer_token = %s"
                val = (farmer_token,)
                cursor.execute(sql, val)
                result = cursor.fetchall()

                for y in result:
                    sn = ''.join(y)
                    try:
                        sql = "INSERT INTO blacklist (fingerprint, serial_num) VALUES (%s, %s)"
                        val = (fingerprint, sn)
                        cursor.execute(sql, val)

                    except:
                        print("Failed inserting SN")
                        raise LambdaException(409)

            except:
                print("Failed blacklist")
                raise LambdaException(400)

        # payout?
        # drop records?

    except:
        print("end")
        raise LambdaException(400)


    db.commit()

    cursor.close()
    db.close()


    return {
        'body': {
          "message": "Blacklisted successfully."
        }
    }