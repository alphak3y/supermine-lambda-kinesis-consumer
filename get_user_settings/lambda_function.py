import mysql.connector
from enum import Enum
import os
from lambda_cache import secrets_manager

class LambdaException(Exception):
    pass

class Users(Enum):
    user_name        = 0
    password         = 1
    xch_balance      = 2
    xch_fee_status   = 3
    xch_reward_addr  = 4
    xch_auto_payout  = 5
    banned           = 6
    gas              = 7
    xch_lifetime_bal = 8
    xfx_balance      = 9
    xfx_fee_status   = 10
    xfx_reward_addr  = 11 
    xfx_auto_payout  = 12
    xfx_lifetime_bal = 13

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    token = event["login_token"]

    if token is None:
        raise LambdaException("400")

    result = None
    username = ""

    try:
        sql = "SELECT user_name FROM sessions WHERE login_token = %s"
        val = (token,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        username = result[0]
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    try:
        sql = "SELECT * FROM users WHERE user_name = %s"
        val = (username,)
        cursor.execute(sql, val)
        result = cursor.fetchone()
        cursor.close()
        db.close()
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException("404")

    return {
        "username": username,
        "xch_reward_addr": result[Users.xch_reward_addr.value],
        "xfx_reward_addr": result[Users.xfx_reward_addr.value],
        "xch_threshold": result[Users.xch_auto_payout.value],
        "xfx_threshold": result[Users.xfx_auto_payout.value]
    }