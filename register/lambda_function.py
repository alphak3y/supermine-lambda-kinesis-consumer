import mysql.connector
import os
#import boto3
from lambda_cache import secrets_manager
#from botocore.exceptions import ClientError

class LambdaException(Exception):
    pass

@secrets_manager.cache(name='rds_username', max_age_in_seconds=3600)
@secrets_manager.cache(name='rds_password', max_age_in_seconds=3600)
def lambda_handler(event, context):
    
    rds_username = getattr(context,'rds_username')
    rds_password = getattr(context,'rds_password')

    db = mysql.connector.connect(
        host=os.environ['db_host'],
        user=rds_username,
        password=rds_password,
        database=os.environ['db_database'],
        port=os.environ['db_port']
    )
    cursor = db.cursor()

    # check for empty values
    if event["username"] is None or event["password"] is None or event["xch_reward_addr"] is None:
        LambdaException(404)

    username = event["username"]
    password = event["password"]
    reward_addr = event["xch_reward_addr"]

    try:
        sql = "INSERT INTO users (USER_NAME, PASSWORD, REWARD_ADDR) VALUES (%s, %s, %s)"
        val = (username, password, reward_addr)
        cursor.execute(sql, val)
        db.commit()
        cursor.close()
        db.close()
        return {
            'body': {
              'message': 'User registration successful'
            }
        }
    except mysql.connector.Error:
        cursor.close()
        db.close()
        raise LambdaException(400)
